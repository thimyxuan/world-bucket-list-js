/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/data.js":
/*!*********************!*\
  !*** ./src/data.js ***!
  \*********************/
/*! exports provided: data */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "data", function() { return data; });

const data = [
  {
    id: 1,
    title: "Se perdre dans les champs de lavande",
    imagePath: "https://media.routard.com/image/29/0/lavande-provence.1486290.w740.jpg",
    done: false,
    link: "https://www.routard.com/mag_week_end/71/en_provence_sur_les_routes_de_la_lavande.htm",
    coordinates: { lat: 43.894916, lng: 6.129254 }
  },
  {
    id: 2,
    title: "Retour aux origines : le Vietnam",
    imagePath: "https://lepetitjournal.com/sites/default/files/styles/main_article/public/2020-03/destination.jpg?itok=VI8QcCvY",
    done: false,
    link: "https://www.ivivu.com/blog/2014/10/du-lich-phan-thiet-cam-nang-tu-a-den-z/",
    coordinates: { lat: 10.980269, lng: 108.261268 }
  },
  {
    id: 3,
    title: "Sur les pas d'Elena Ferrante, Naples",
    imagePath: "https://ifly-selections-axaxzmkc.netdna-ssl.com/10301486-klm-selections/content/image/_f_large/FF_Naples_01_header-1554304178.jpg",
    done: true,
    link: "#",
    coordinates: { lat: 40.848848, lng: 14.267041 }
  },
  {
    id: 4,
    title: "Ma quête spirituelle dans le désert du Sahara",
    imagePath: "https://galiciatravels.com/wp-content/uploads/2018/04/GAL_DES_16.jpg",
    done: false,
    link: "#",
    coordinates: { lat: 31.097744, lng: -4.008488 }
  },
  {
    id: 5,
    title: "Istanbul ville aux mille lumières",
    imagePath: "https://turkishairlines.ssl.cdn.sdlmedia.com/637128523988699111JF.jpg",
    done: false,
    link: "#",
    coordinates: { lat: 41.008542, lng: 28.980222 }
  },
  {
    id: 6,
    title: "À la limite du monde",
    imagePath: "https://i.pinimg.com/originals/9a/b7/0f/9ab70f11c57367fe8ba466bedc404dd3.jpg",
    done: false,
    link: "#",
    coordinates: { lat: 61.247870, lng: -47.123815 }
  },
  {
    id: 7,
    title: "Se baigner dans le blue lagoon",
    imagePath: "https://i1.wp.com/hoteletlodge.fr/site2/wp-content/uploads/2014/08/Blue-Lagoon-Islande.jpg?fit=1600%2C1066&ssl=1",
    done: false,
    link: "#",
    coordinates: { lat: 64.603870, lng: -18.511150 }
  }
] 



/***/ }),

/***/ "./src/dream.js":
/*!**********************!*\
  !*** ./src/dream.js ***!
  \**********************/
/*! exports provided: buildAllDreams */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buildAllDreams", function() { return buildAllDreams; });
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data */ "./src/data.js");
/* harmony import */ var _map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./map */ "./src/map.js");




const dreamsContainer = document.querySelector("#dreams-container");

function buildAllDreams() {
  while(dreamsContainer.hasChildNodes()) { // je fais ceci pour être sûr que l'on ait pas de doublon dans les dreams : je vide le dreams-container
    dreamsContainer.removeChild(dreamsContainer.lastChild); // ici j'enlève le dernier enfant jusqu'à ce qu'il n'y ait plus d'enfant
  }
  _data__WEBPACK_IMPORTED_MODULE_0__["data"].forEach(buildOneDream);
  addDreamListener();
  addDreamButtonListener();
}

function buildOneDream(dream) {
  const dreamElement = document.createElement("div"); // je crée un élément dans le DOM
  dreamElement.innerHTML = `<div class="card text-center" id="${dream.id}">
    <div class="card-header font-weight-light">
      ${dream.title}
    </div>
    <img src="${dream.imagePath}" class="card-img-top" alt="">
    <div class="card-body">
      <a href="#" class="button-letsgo btn btn-${dream.done ? "secondary" : "danger" } font-weight-bold btn-block">${dream.done ? "Je veux le refaire" : "Je me lance !" }</a>
    </div>
    <div class="card-footer text-muted text-right">
      <a href="#" class="btn btn-outline-secondary btn-sm button-visit">Visiter</a>
      <a href="${dream.link}" target="_blank" class="btn btn-outline-dark btn-sm">Plus d'info</a>
    </div>
  </div>`;
  dreamsContainer.appendChild(dreamElement); // on ajoute l'élément créé dans notre html
  Object(_map__WEBPACK_IMPORTED_MODULE_1__["addMarkerOnMap"])(dream);
}
// Note : normalement le id devrait être stocké dans un attribut type data-quelquechose = ""
// Ici on l'a mis dans un id = "" 

function addDreamListener() {
  document.querySelectorAll(".button-visit").forEach( item => {
    item.addEventListener("click", event => {
      visitDream(item.parentElement.parentElement.getAttribute("id")); // on récupére le id du dream
    })
  });
}
// Note : document.querySelectorAll(".button-visit") retourne un array : je peux faire un foreach dessus

function visitDream(dreamId) {
  let position = _data__WEBPACK_IMPORTED_MODULE_0__["data"].filter(item => item.id == dreamId )[0].coordinates;
  Object(_map__WEBPACK_IMPORTED_MODULE_1__["visitDreamOnMap"])(position);
}
// Note : la méthode filter permet de filtrer les éléments d'un array
// -> la méthode filter retourne un array
// -> On demande l'élément à l'index 0 pour récupérer la donnée

function addDreamButtonListener() {
  document.querySelectorAll(".button-letsgo").forEach( item => {
    item.addEventListener("click", event => {
      toggleDreamDone(item.parentElement.parentElement.getAttribute("id"));
    })
  });
}

function toggleDreamDone(dreamId) {
  let selectedDream = _data__WEBPACK_IMPORTED_MODULE_0__["data"].filter( item => item.id == dreamId )[0];
  selectedDream.done ?  Object(_map__WEBPACK_IMPORTED_MODULE_1__["resetMap"])() : Object(_map__WEBPACK_IMPORTED_MODULE_1__["zoomOnDream"])(selectedDream.coordinates);
  selectedDream.done = !selectedDream.done; // une écriture plus courte que :
  //selectedDream.done == false ? selectedDream.done = true : selectedDream.done = false ;
  buildAllDreams();
}



/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _map__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./map */ "./src/map.js");
/* harmony import */ var _dream__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dream */ "./src/dream.js");




function init() {
  Object(_map__WEBPACK_IMPORTED_MODULE_0__["initMap"])();
  Object(_dream__WEBPACK_IMPORTED_MODULE_1__["buildAllDreams"])();
}

window.init = init; 
// ceci permet d'attacher la fonction init() ci-dessus au scope global (en l'attachant à window) 
//-> pour pouvoir l'appeler en callback ensuite

/***/ }),

/***/ "./src/map.js":
/*!********************!*\
  !*** ./src/map.js ***!
  \********************/
/*! exports provided: initMap, addMarkerOnMap, visitDreamOnMap, zoomOnDream, resetMap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initMap", function() { return initMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addMarkerOnMap", function() { return addMarkerOnMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "visitDreamOnMap", function() { return visitDreamOnMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "zoomOnDream", function() { return zoomOnDream; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resetMap", function() { return resetMap; });

let map;
let panorama;
const panoramaElement = document.querySelector("#panorama");

const resetMapButton = document.querySelector("#reset");
const backToMapButton = document.querySelector("#backtomap");

function initMap() {

  const lyon = {lat: 45.777005, lng: 4.854873};

  map = new google.maps.Map(document.getElementById('map'), {
    center: lyon,
    zoom: 3,
    streetViewControl: false // ça enlève le petit bonhomme streetview en bas à droite
    // styles: // ajouter le code JSON ici pour styliser ma carte
    // -> https://mapstyle.withgoogle.com/
  });

  panorama = new google.maps.StreetViewPanorama(
    document.getElementById('panorama'), {
      position: {lat: 45.773277, lng: 4.854595},
      pov: {
        heading: 34,
        pitch: 10
      }
  });

  const home = new google.maps.Marker({
    position: lyon,
    map: map,
    icon: {
      url: "images/home.svg",
      scaledSize: new google.maps.Size(30,30)
    }
  });

  home.addListener('click', function() {
    visitDreamOnMap(home.position);
  });
  
  addMapListener();
  panoramaElement.style.display = "none";
  backToMapButton.style.display = "none";
}

function addMapListener() {
  resetMapButton.addEventListener("click", resetMap);
}

function addMarkerOnMap(dream) {
  const marker = new google.maps.Marker({
    position: dream.coordinates,
    map: map,
    icon: {
      url: dream.done ? "images/marker-done.svg" : "images/marker.svg",
      scaledSize: new google.maps.Size(30,30)
    },
  });
  marker.addListener('click', function() {
    zoomOn(marker.getPosition());
  });
}

function zoomOn(position) {
  map.setZoom(20);
  map.setCenter(position);
  map.setMapTypeId("satellite");
}

function zoomOnDream(position) {
  map.setZoom(5);
  map.setCenter(position);
}

function resetMap() {
  map.setZoom(3);
  map.setCenter({lat: 45.777005, lng: 4.854873});
  map.setMapTypeId("roadmap")
  resetMapButton.style.display ="block";
}

function visitDreamOnMap(position) {
  panoramaElement.style.display = "block";
  backToMapButton.style.display = "block";
  resetMapButton.style.display ="none";
  panorama.setPosition(position);
  addPanoramaListener();
}

function addPanoramaListener() {
  backToMapButton.addEventListener("click", backToMap);
}

function backToMap() {
  panoramaElement.style.display = "none";
  backToMapButton.style.display = "none";
  resetMap();
}



/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map
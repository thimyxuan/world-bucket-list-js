
const data = [
  {
    id: 1,
    title: "Se perdre dans les champs de lavande",
    imagePath: "https://media.routard.com/image/29/0/lavande-provence.1486290.w740.jpg",
    done: false,
    link: "https://www.routard.com/mag_week_end/71/en_provence_sur_les_routes_de_la_lavande.htm",
    coordinates: { lat: 43.894916, lng: 6.129254 }
  },
  {
    id: 2,
    title: "Retour aux origines : le Vietnam",
    imagePath: "https://lepetitjournal.com/sites/default/files/styles/main_article/public/2020-03/destination.jpg?itok=VI8QcCvY",
    done: false,
    link: "https://www.ivivu.com/blog/2014/10/du-lich-phan-thiet-cam-nang-tu-a-den-z/",
    coordinates: { lat: 10.980269, lng: 108.261268 }
  },
  {
    id: 3,
    title: "Sur les pas d'Elena Ferrante, Naples",
    imagePath: "https://ifly-selections-axaxzmkc.netdna-ssl.com/10301486-klm-selections/content/image/_f_large/FF_Naples_01_header-1554304178.jpg",
    done: true,
    link: "#",
    coordinates: { lat: 40.848848, lng: 14.267041 }
  },
  {
    id: 4,
    title: "Ma quête spirituelle dans le désert du Sahara",
    imagePath: "https://galiciatravels.com/wp-content/uploads/2018/04/GAL_DES_16.jpg",
    done: false,
    link: "#",
    coordinates: { lat: 31.097744, lng: -4.008488 }
  },
  {
    id: 5,
    title: "Istanbul ville aux mille lumières",
    imagePath: "https://turkishairlines.ssl.cdn.sdlmedia.com/637128523988699111JF.jpg",
    done: false,
    link: "#",
    coordinates: { lat: 41.008542, lng: 28.980222 }
  },
  {
    id: 6,
    title: "À la limite du monde",
    imagePath: "https://i.pinimg.com/originals/9a/b7/0f/9ab70f11c57367fe8ba466bedc404dd3.jpg",
    done: false,
    link: "#",
    coordinates: { lat: 61.247870, lng: -47.123815 }
  },
  {
    id: 7,
    title: "Se baigner dans le blue lagoon",
    imagePath: "https://i1.wp.com/hoteletlodge.fr/site2/wp-content/uploads/2014/08/Blue-Lagoon-Islande.jpg?fit=1600%2C1066&ssl=1",
    done: false,
    link: "#",
    coordinates: { lat: 64.603870, lng: -18.511150 }
  }
] 

export {data};
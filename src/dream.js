
import {data} from "./data";
import {addMarkerOnMap, visitDreamOnMap, zoomOnDream, resetMap} from "./map";

const dreamsContainer = document.querySelector("#dreams-container");

function buildAllDreams() {
  while(dreamsContainer.hasChildNodes()) { // je fais ceci pour être sûr que l'on ait pas de doublon dans les dreams : je vide le dreams-container
    dreamsContainer.removeChild(dreamsContainer.lastChild); // ici j'enlève le dernier enfant jusqu'à ce qu'il n'y ait plus d'enfant
  }
  data.forEach(buildOneDream);
  addDreamListener();
  addDreamButtonListener();
}

function buildOneDream(dream) {
  const dreamElement = document.createElement("div"); // je crée un élément dans le DOM
  dreamElement.innerHTML = `<div class="card text-center" id="${dream.id}">
    <div class="card-header font-weight-light">
      ${dream.title}
    </div>
    <img src="${dream.imagePath}" class="card-img-top" alt="">
    <div class="card-body">
      <a href="#" class="button-letsgo btn btn-${dream.done ? "secondary" : "danger" } font-weight-bold btn-block">${dream.done ? "Je veux le refaire" : "Je me lance !" }</a>
    </div>
    <div class="card-footer text-muted text-right">
      <a href="#" class="btn btn-outline-secondary btn-sm button-visit">Visiter</a>
      <a href="${dream.link}" target="_blank" class="btn btn-outline-dark btn-sm">Plus d'info</a>
    </div>
  </div>`;
  dreamsContainer.appendChild(dreamElement); // on ajoute l'élément créé dans notre html
  addMarkerOnMap(dream);
}
// Note : normalement le id devrait être stocké dans un attribut type data-quelquechose = ""
// Ici on l'a mis dans un id = "" 

function addDreamListener() {
  document.querySelectorAll(".button-visit").forEach( item => {
    item.addEventListener("click", event => {
      visitDream(item.parentElement.parentElement.getAttribute("id")); // on récupére le id du dream
    })
  });
}
// Note : document.querySelectorAll(".button-visit") retourne un array : je peux faire un foreach dessus

function visitDream(dreamId) {
  let position = data.filter(item => item.id == dreamId )[0].coordinates;
  visitDreamOnMap(position);
}
// Note : la méthode filter permet de filtrer les éléments d'un array
// -> la méthode filter retourne un array
// -> On demande l'élément à l'index 0 pour récupérer la donnée

function addDreamButtonListener() {
  document.querySelectorAll(".button-letsgo").forEach( item => {
    item.addEventListener("click", event => {
      toggleDreamDone(item.parentElement.parentElement.getAttribute("id"));
    })
  });
}

function toggleDreamDone(dreamId) {
  let selectedDream = data.filter( item => item.id == dreamId )[0];
  selectedDream.done ?  resetMap() : zoomOnDream(selectedDream.coordinates);
  selectedDream.done = !selectedDream.done; // une écriture plus courte que :
  //selectedDream.done == false ? selectedDream.done = true : selectedDream.done = false ;
  buildAllDreams();
}

export {buildAllDreams};
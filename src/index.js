
import {initMap} from "./map";
import {buildAllDreams} from "./dream";

function init() {
  initMap();
  buildAllDreams();
}

window.init = init; 
// ceci permet d'attacher la fonction init() ci-dessus au scope global (en l'attachant à window) 
//-> pour pouvoir l'appeler en callback ensuite
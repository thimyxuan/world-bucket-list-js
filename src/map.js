
let map;
let panorama;
const panoramaElement = document.querySelector("#panorama");

const resetMapButton = document.querySelector("#reset");
const backToMapButton = document.querySelector("#backtomap");

function initMap() {

  const lyon = {lat: 45.777005, lng: 4.854873};

  map = new google.maps.Map(document.getElementById('map'), {
    center: lyon,
    zoom: 3,
    streetViewControl: false // ça enlève le petit bonhomme streetview en bas à droite
    // styles: // ajouter le code JSON ici pour styliser ma carte
    // -> https://mapstyle.withgoogle.com/
  });

  panorama = new google.maps.StreetViewPanorama(
    document.getElementById('panorama'), {
      position: {lat: 45.773277, lng: 4.854595},
      pov: {
        heading: 34,
        pitch: 10
      }
  });

  const home = new google.maps.Marker({
    position: lyon,
    map: map,
    icon: {
      url: "images/home.svg",
      scaledSize: new google.maps.Size(30,30)
    }
  });

  home.addListener('click', function() {
    visitDreamOnMap(home.position);
  });
  
  addMapListener();
  panoramaElement.style.display = "none";
  backToMapButton.style.display = "none";
}

function addMapListener() {
  resetMapButton.addEventListener("click", resetMap);
}

function addMarkerOnMap(dream) {
  const marker = new google.maps.Marker({
    position: dream.coordinates,
    map: map,
    icon: {
      url: dream.done ? "images/marker-done.svg" : "images/marker.svg",
      scaledSize: new google.maps.Size(30,30)
    },
  });
  marker.addListener('click', function() {
    zoomOn(marker.getPosition());
  });
}

function zoomOn(position) {
  map.setZoom(20);
  map.setCenter(position);
  map.setMapTypeId("satellite");
}

function zoomOnDream(position) {
  map.setZoom(5);
  map.setCenter(position);
}

function resetMap() {
  map.setZoom(3);
  map.setCenter({lat: 45.777005, lng: 4.854873});
  map.setMapTypeId("roadmap")
  resetMapButton.style.display ="block";
}

function visitDreamOnMap(position) {
  panoramaElement.style.display = "block";
  backToMapButton.style.display = "block";
  resetMapButton.style.display ="none";
  panorama.setPosition(position);
  addPanoramaListener();
}

function addPanoramaListener() {
  backToMapButton.addEventListener("click", backToMap);
}

function backToMap() {
  panoramaElement.style.display = "none";
  backToMapButton.style.display = "none";
  resetMap();
}

export {initMap, addMarkerOnMap, visitDreamOnMap, zoomOnDream, resetMap};
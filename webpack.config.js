const path = require("path");

module.exports = {
  watch: true, 
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devtool: "source-map", // c'est un outil pour débuger -> donne une carte entre mes fichiers sources et le fichier final généré par webpack bundle.js 
  mode: 'development',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    open: true
  }
}